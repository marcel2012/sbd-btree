#include "record_layer.h"
#include "disk_layer.h"


void push_record(struct tape *tape, const RECORD_TYPE *record) {
    long index = tape->last_record + 1;

    update_record(tape, record, index);

    tape->last_record++;
}

void update_record(struct tape *tape, const RECORD_TYPE *record, long index) {
    long page_index = index / RECORDS_PER_PAGE;
    long record_index = index % RECORDS_PER_PAGE;

    if (page_index != tape->buffer_page_index) {
        write_page_if_edited(tape);
        get_record(tape, index);
    }

    tape->records[record_index] = *record;
    tape->buffer_edited = true;

    // printf("push %s %d\n", tape->file_name, length_square(record));
}

RECORD_TYPE *get_record(struct tape *tape, const long index) {
    long page_index = index / RECORDS_PER_PAGE;
    long record_index = index % RECORDS_PER_PAGE;

    if (page_index != tape->buffer_page_index) {
        write_page_if_edited(tape);
        if (!read_page(tape, page_index)) {
            return NULL;
        }
    }

    if (index > tape->last_record) {
        return NULL;
    }


    RECORD_TYPE *record = tape->records + record_index;

    // printf("get %s %d\n", tape->file_name, length_square(record));

    return record;
}
