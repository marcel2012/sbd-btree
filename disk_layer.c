#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "disk_layer.h"


int write_page_counter;
int read_page_counter;

int get_write_page_counter() {
    return write_page_counter;
}

int get_read_page_counter() {
    return read_page_counter;
}

void reset_counters() {
    write_page_counter = 0;
    read_page_counter = 0;
}

struct tape *prepare_file(const char *file_name) {
    struct tape *tape = calloc(1, sizeof(struct tape));
    assert(tape != NULL);

    strcpy(tape->file_name, file_name);

    tape->records = (RECORD_TYPE *) tape->buffer;
    tape->buffer_edited = false;

    return tape;
}

void erase_tape_and_start_writing(struct tape *tape) {
    tape->file = fopen(tape->file_name, "wb+");

    assert(tape->file != NULL);

    tape->last_record = -1;
    tape->buffer_page_index = -1;
    tape->buffer_edited = false;
}

void write_page(const struct tape *tape) {
    write_page_counter++;

    fseek(tape->file, PAGE_SIZE * tape->buffer_page_index, SEEK_SET);
    size_t s = fwrite(tape->buffer, PAGE_SIZE, 1, tape->file);

    assert(s == 1);
}

void write_page_if_edited(struct tape *tape) {
    if (tape->buffer_edited) {
        write_page(tape);
        tape->buffer_edited = false;
    }
}

void close_file(struct tape *tape) {
    write_page_if_edited(tape);

    int s = fclose(tape->file);
    assert(s == 0);

    free(tape);
}

bool read_page(struct tape *t, const long page_index) {
    assert(t->buffer_edited == false);

    int status = fseek(t->file, PAGE_SIZE * page_index, SEEK_SET);
    assert(status == 0);

    size_t s = fread(t->buffer, PAGE_SIZE, 1, t->file);

    t->buffer_page_index = page_index;

    if (s != 1) {
        return false;
    }
    read_page_counter++;
    return true;
}
