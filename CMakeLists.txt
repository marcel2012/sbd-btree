cmake_minimum_required(VERSION 3.11)
project(sbd C)

set(CMAKE_C_STANDARD 11)

add_executable(sbd main.c disk_layer.c record_layer.c)

add_definitions(-Wall)
add_definitions(-DPRINT_TAPES)