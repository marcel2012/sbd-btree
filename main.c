#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include "disk_layer.h"
#include "record_layer.h"

#define BTREE_D 21

struct index_list {
    int index;
    struct index_list *next;
};

struct btree_record {
    int key;
    int address;
};

struct btree_page {
    int records_count;
    struct btree_record records[2 * BTREE_D];
    int pointers[2 * BTREE_D + 1];
};

struct btree_page_cache {
    int index;
    bool modified;
    struct btree_page page;
    int access_time;
};

struct btree {
    FILE *file;
    int last_page_index;
    struct btree_page_cache *cache;
    int cache_size;
    int head_index;

    struct index_list *removed_pages;
};

int cache_access_time = 0;

void initialize_cache(struct btree_page_cache *cache) {
    cache->index = -1;
    cache->modified = false;
    cache->access_time = cache_access_time++;
}

void resize_btree_caches(struct btree *tree, int newSize) {
    tree->cache = realloc(tree->cache, newSize * sizeof(struct btree_page_cache));
    assert(tree->cache != NULL);

    for (int i = tree->cache_size; i < newSize; i++) {
        initialize_cache(tree->cache + i);
    }

    tree->cache_size = newSize;
}

struct btree create_btree(const char filename[]) {
    struct btree tree;
    tree.file = fopen(filename, "wb+");

    assert(tree.file != NULL);

    tree.removed_pages = NULL;
    tree.cache = NULL;

    tree.head_index = -1;

    tree.last_page_index = -1;
    tree.cache_size = 0;
    resize_btree_caches(&tree, 1);

    return tree;
}

int btree_write_page_counter;
int btree_read_page_counter;

void write_btree_page(struct btree *tree, struct btree_page_cache *cache) {
    assert(cache->modified == true);

    fseek(tree->file, PAGE_SIZE * cache->index, SEEK_SET);
    size_t s = fwrite(&cache->page, sizeof(struct btree_page), 1, tree->file);

    assert(s == 1);

    cache->modified = false;
    btree_write_page_counter++;
}

void invalidate_cache(struct btree *tree) {
    for (int i = 0; i < tree->cache_size; i++) {
        if (tree->cache[i].index != -1 && tree->cache[i].modified) {
            write_btree_page(tree, tree->cache + i);
            tree->cache->modified = false;
            tree->cache->index = -1;
        }
    }
}

void close_btree(struct btree *tree) {
    invalidate_cache(tree);

    free(tree->cache);
    int s = fclose(tree->file);

    assert(s == 0);

    while (tree->removed_pages != NULL) {
        struct index_list *next = tree->removed_pages->next;
        free(tree->removed_pages);
        tree->removed_pages = next;
    }
}

struct btree_page_cache *get_empty_cache(struct btree *tree) {
    for (int i = 0; i < tree->cache_size; i++) {
        if (tree->cache[i].index == -1) {
            assert(tree->cache[i].modified == false);

            tree->cache[i].page.records_count = 0;
            for (int j = 0; j < 2 * BTREE_D + 1; j++) {
                tree->cache[i].page.pointers[j] = -1;
            }

            tree->cache[i].access_time = cache_access_time++;
            return tree->cache + i;
        }
    }
    int cache_to_delete = 0;
    for (int i = 0; i < tree->cache_size; i++) {
        if (tree->cache[i].access_time < tree->cache[cache_to_delete].access_time) {
            cache_to_delete = i;
        }
    }

    if (tree->cache[cache_to_delete].modified) {
        write_btree_page(tree, tree->cache + cache_to_delete);
    }

    tree->cache[cache_to_delete].page.records_count = 0;
    for (int j = 0; j < 2 * BTREE_D + 1; j++) {
        tree->cache[cache_to_delete].page.pointers[j] = -1;
    }

    tree->cache[cache_to_delete].access_time = cache_access_time++;

    return tree->cache + cache_to_delete;
}

struct btree_page_cache *create_empty_cache(struct btree *tree) {
    struct btree_page_cache *cache = get_empty_cache(tree);

    if (tree->removed_pages != NULL) {
        cache->index = tree->removed_pages->index;
        struct index_list *next = tree->removed_pages->next;
        free(tree->removed_pages);
        tree->removed_pages = next;
    } else {
        tree->last_page_index++;
        cache->index = tree->last_page_index;
    }
    return cache;
}

void remove_page(struct btree *tree, struct btree_page_cache *cache) {
    assert(cache->index >= 0);

    struct index_list *new = malloc(sizeof(struct index_list));
    assert(new != NULL);

    new->index = cache->index;
    new->next = tree->removed_pages;
    tree->removed_pages = new;

    cache->modified = false;
    cache->index = -1;
}

struct btree_page_cache *read_btree_page(struct btree *tree, int index) {
    assert(index <= tree->last_page_index);

    for (int i = 0; i < tree->cache_size; i++) {
        if (tree->cache[i].index == index) {
            tree->cache[i].access_time = cache_access_time++;
            return tree->cache + i;
        }
    }

    struct btree_page_cache *cache = get_empty_cache(tree);

    int status = fseek(tree->file, PAGE_SIZE * index, SEEK_SET);
    assert(status == 0);

    size_t s = fread(&cache->page, sizeof(struct btree_page), 1, tree->file);

    btree_read_page_counter++;

    if (s != 1) {
        return false;
    }
    cache->index = index;
    return cache;
}

struct btree_search_result {
    struct btree_record *record;
    struct btree_page_cache *page_cache;
    struct btree_page_cache *actual_cache;
};

struct btree_search_result
search_internal(struct btree *tree, int key, int page_index, struct btree_page_cache *last_page_cache) {
    if (page_index < 0) {
        struct btree_search_result result;
        result.record = NULL;
        result.page_cache = last_page_cache;
        result.actual_cache = NULL;
        //printf("Not found %d\n", key);
        return result;
    }
    struct btree_page_cache *page_cache = read_btree_page(tree, page_index);

    for (int i = 0; i < page_cache->page.records_count; i++) {
        if (page_cache->page.records[i].key == key) {
            //printf("Found %d\n", key);
            struct btree_search_result result;
            result.record = page_cache->page.records + i;
            result.page_cache = last_page_cache;
            result.actual_cache = page_cache;
            return result;
        } else if (key < page_cache->page.records[i].key) {
            return search_internal(tree, key, page_cache->page.pointers[i], page_cache);
        }
    }
    return search_internal(tree, key, page_cache->page.pointers[page_cache->page.records_count],
                           page_cache);
}

struct btree_search_result search(struct btree *tree, int key) {
    return search_internal(tree, key, tree->head_index, NULL);
}

bool move(struct btree_record *destination, struct btree_page *page, int index_in_page, struct btree_record *new_source,
          int nl, int nr, bool *inserting_check, int *dl, int *dr) {
    assert(dl != NULL);
    assert(dr != NULL);

    struct btree_record *list_source = page->records + index_in_page;
    if (index_in_page >= page->records_count || (*inserting_check && new_source->key < list_source->key)) {
        *destination = *new_source;
        *inserting_check = false;
        *dl = nl;
        *dr = nr;
        return 0;
    } else {
        int ll = page->pointers[index_in_page];
        int lr = page->pointers[index_in_page + 1];
        *destination = *list_source;
        *dl = ll;
        *dr = lr;
        return 1;
    }
}

struct btree_page_cache *get_record_key_parent(struct btree *tree, int key) {
    struct btree_search_result search_result = search(tree, key);
    return search_result.page_cache;
}

struct btree_page_cache *get_parent(struct btree *tree, struct btree_page_cache *page_cache) {
    struct btree_page_cache *parent_cache = get_record_key_parent(tree, page_cache->page.records[0].key);
    return parent_cache;
}

struct btree_record *get_left_parent_record(struct btree *tree, struct btree_page_cache *page_cache) {
    struct btree_page_cache *parent = get_parent(tree, page_cache);
    if (parent != NULL) {
        parent->modified = true;
        for (int i = 0; i < parent->page.records_count; i++) {
            if (parent->page.pointers[i + 1] == page_cache->index) {
                return parent->page.records + i;
            }
        }
    }
    return NULL;
}

struct btree_record *get_right_parent_record(struct btree *tree, struct btree_page_cache *page_cache) {
    struct btree_page_cache *parent = get_parent(tree, page_cache);
    if (parent != NULL) {
        parent->modified = true;
        for (int i = 0; i < parent->page.records_count; i++) {
            if (parent->page.pointers[i] == page_cache->index) {
                return parent->page.records + i;
            }
        }
    }
    return NULL;
}

int get_left_sibling(struct btree *tree, struct btree_page_cache *page_cache) {
    struct btree_page_cache *parent = get_parent(tree, page_cache);
    if (parent != NULL) {
        for (int i = 0; i < parent->page.records_count; i++) {
            if (parent->page.pointers[i + 1] == page_cache->index) {
                return parent->page.pointers[i];
            }
        }
    }
    return -1;
}

int get_right_sibling(struct btree *tree, struct btree_page_cache *page_cache) {
    struct btree_page_cache *parent = get_parent(tree, page_cache);
    if (parent != NULL) {
        for (int i = 0; i < parent->page.records_count; i++) {
            if (parent->page.pointers[i] == page_cache->index) {
                return parent->page.pointers[i + 1];
            }
        }
    }
    return -1;
}

void insert_easy(struct btree_page *page, struct btree_record *record, int nl, int nr) {
    int place = 0;
    for (; place < page->records_count && record->key > page->records[place].key; place++);
    for (int i = page->records_count; i > place; i--) {
        page->records[i] = page->records[i - 1];
        page->pointers[i + 1] = page->pointers[i];
    }
    page->records[place] = *record;
    page->pointers[place] = nl;
    page->pointers[place + 1] = nr;
    page->records_count++;
}

void
compensate(struct btree_page_cache *left_cache, struct btree_record *middle, struct btree_page_cache *right_cache,
           struct btree_record *new_record, int nl, int nr, bool left, int destination_count) {
    bool inserting_check = new_record != NULL;

    if (left) {
        while (left_cache->page.records_count < destination_count) {
            left_cache->page.records[left_cache->page.records_count] = *middle;
            if (inserting_check && new_record->key < right_cache->page.records[0].key) {
                *middle = *new_record;
                left_cache->page.pointers[left_cache->page.records_count + 1] = nl;
                inserting_check = false;
            } else {
                *middle = right_cache->page.records[0];
                left_cache->page.pointers[left_cache->page.records_count + 1] = right_cache->page.pointers[0];

                right_cache->page.pointers[0] = right_cache->page.pointers[1];
                for (int i = 1; i < right_cache->page.records_count; i++) {
                    right_cache->page.pointers[i] = right_cache->page.pointers[i + 1];
                    right_cache->page.records[i - 1] = right_cache->page.records[i];
                }
                if (right_cache->page.records_count) {
                    right_cache->page.records_count--;
                }
            }
            left_cache->page.records_count++;
        }

        if (inserting_check) {
            insert_easy(&right_cache->page, new_record, nl, nr);
        }
    } else {
        while (right_cache->page.records_count < destination_count) {
            right_cache->page.pointers[right_cache->page.records_count +
                                       1] = right_cache->page.pointers[right_cache->page.records_count];
            for (int i = right_cache->page.records_count; i > 0; i--) {
                right_cache->page.pointers[i] = right_cache->page.pointers[i - 1];
                right_cache->page.records[i] = right_cache->page.records[i - 1];
            }
            right_cache->page.records[0] = *middle;
            if (inserting_check && new_record->key > left_cache->page.records[left_cache->page.records_count - 1].key) {
                *middle = *new_record;
                right_cache->page.pointers[0] = nr;
                left_cache->page.pointers[left_cache->page.records_count] = nl;
                inserting_check = false;
            } else {
                *middle = left_cache->page.records[left_cache->page.records_count - 1];
                right_cache->page.pointers[0] = left_cache->page.pointers[left_cache->page.records_count];
                if (left_cache->page.records_count) {
                    left_cache->page.records_count--;
                }
            }
            right_cache->page.records_count++;
        }

        if (inserting_check) {
            insert_easy(&left_cache->page, new_record, nl, nr);
        }
    }
}

void
insert_precise(struct btree *tree, struct btree_record *record, struct btree_page_cache *page_cache, int left_pointer,
               int right_pointer) {
    if (page_cache == NULL) {
        resize_btree_caches(tree, tree->cache_size + 1);

        page_cache = create_empty_cache(tree);
        tree->head_index = page_cache->index;
    }

    assert(page_cache != NULL);

    page_cache->modified = true;
    struct btree_page *page = &page_cache->page;

    assert(page != NULL);

    if (page->records_count < 2 * BTREE_D) {
        insert_easy(page, record, left_pointer, right_pointer);
        return;
    } else {
        assert(page->records_count == 2 * BTREE_D);

        // compensation

        int left = get_left_sibling(tree, page_cache);
        if (left >= 0) {
            struct btree_page_cache *left_cache = read_btree_page(tree, left);
            if (left_cache->page.records_count < 2 * BTREE_D) {
                left_cache->modified = true;
                struct btree_record *old_middle = get_left_parent_record(tree, page_cache);
                assert(old_middle != NULL);

                int destination_count = (left_cache->page.records_count + page_cache->page.records_count + 2) / 2;
                compensate(left_cache, old_middle, page_cache, record, left_pointer,
                           right_pointer, true, destination_count);
                return;
            }
        }
        page_cache->access_time = cache_access_time++; // keep this page in memory

        int right = get_right_sibling(tree, page_cache);
        if (right >= 0) {
            struct btree_page_cache *right_cache = read_btree_page(tree, right);
            if (right_cache->page.records_count < 2 * BTREE_D) {
                right_cache->modified = true;
                struct btree_record *old_middle = get_right_parent_record(tree, page_cache);
                assert(old_middle != NULL);

                int destination_count = (right_cache->page.records_count + page_cache->page.records_count + 2) / 2;
                compensate(page_cache, old_middle, right_cache, record, left_pointer,
                           right_pointer, false, destination_count);
                return;
            }
        }

        // split

        struct btree_page_cache *new_cache = create_empty_cache(tree);

        new_cache->modified = true;

        int moved = 0;
        int old_index = 0;
        bool inserting_check = true;
        while (moved < BTREE_D) {
            old_index += move(new_cache->page.records + moved, &page_cache->page, old_index, record, left_pointer,
                              right_pointer, &inserting_check, new_cache->page.pointers + moved,
                              new_cache->page.pointers + moved + 1);
            moved++;
        }

        new_cache->page.records_count = moved;

        struct btree_record middle_record;
        old_index += move(&middle_record, &page_cache->page, old_index, record, left_pointer,
                          right_pointer, &inserting_check, new_cache->page.pointers + moved, page_cache->page.pointers);

        moved = 0;
        while (moved < BTREE_D) {
            old_index += move(page_cache->page.records + moved, &page_cache->page, old_index, record, left_pointer,
                              right_pointer, &inserting_check, page_cache->page.pointers + moved,
                              page_cache->page.pointers + moved + 1);
            moved++;
        }

        page_cache->page.records_count = moved;

        left_pointer = new_cache->index;
        right_pointer = page_cache->index;

        insert_precise(tree, &middle_record, get_record_key_parent(tree, page_cache->page.records[0].key), left_pointer,
                       right_pointer);
    }
}

void remove_precise(struct btree *tree, int key, struct btree_page_cache *page_cache) {
    assert(page_cache != NULL);

    page_cache->modified = true;
    struct btree_page *page = &page_cache->page;

    assert(page != NULL);

    int place = 0;
    for (; place < page->records_count && key > page->records[place].key; place++);

    assert(key == page->records[place].key);

    if (page->pointers[place + 1] >= 0) {
        struct btree_search_result leaf = search_internal(tree, -1, page->pointers[place + 1], NULL);

        assert(leaf.page_cache != NULL);

        page_cache->access_time = cache_access_time++; // keep this page in memory

        page->records[place] = leaf.page_cache->page.records[0];
        page_cache = leaf.page_cache;
        page = &page_cache->page;

        page_cache->modified = true;
        place = 0;
    }

    remove:

    page->pointers[place] = page->pointers[place + 1];
    for (int i = place + 1; i < page->records_count; i++) {
        page->records[i - 1] = page->records[i];
        page->pointers[i] = page->pointers[i + 1];
    }
    page->records_count--;

    assert(page->records_count >= 0);

    if (tree->head_index == page_cache->index) {
        if (page->records_count == 0) {
            tree->head_index = page_cache->page.pointers[0];
            remove_page(tree, page_cache);
        }
        return;
    }


    if (page->records_count < BTREE_D) {
        assert(page->records_count == BTREE_D - 1);
        // compensation

        int left = get_left_sibling(tree, page_cache);
        if (left >= 0) {
            struct btree_page_cache *left_cache = read_btree_page(tree, left);
            if (left_cache->page.records_count > BTREE_D) {
                left_cache->modified = true;
                struct btree_record *old_middle = get_left_parent_record(tree, page_cache);
                assert(old_middle != NULL);

                int destination_count = (left_cache->page.records_count + page_cache->page.records_count + 1) / 2;
                compensate(left_cache, old_middle, page_cache, NULL, -1, -1, false, destination_count);
                return;
            }
        }
        page_cache->access_time = cache_access_time++; // keep this page in memory

        int right = get_right_sibling(tree, page_cache);
        if (right >= 0) {
            struct btree_page_cache *right_cache = read_btree_page(tree, right);
            if (right_cache->page.records_count > BTREE_D) {
                right_cache->modified = true;
                struct btree_record *old_middle = get_right_parent_record(tree, page_cache);
                assert(old_middle != NULL);

                int destination_count = (right_cache->page.records_count + page_cache->page.records_count + 1) / 2;
                compensate(page_cache, old_middle, right_cache, NULL, -1, -1, true, destination_count);
                return;
            }
        }
        page_cache->access_time = cache_access_time++; // keep this page in memory

        // merge

        if (left >= 0) {
            struct btree_page_cache *left_cache = read_btree_page(tree, left);
            if (left_cache->page.records_count == BTREE_D) {
                left_cache->modified = true;
                struct btree_record *old_middle = get_left_parent_record(tree, page_cache);
                assert(old_middle != NULL);

                int destination_count = (left_cache->page.records_count + page_cache->page.records_count + 1);
                assert(destination_count == 2 * BTREE_D);
                compensate(left_cache, old_middle, page_cache, NULL, -1, -1, false, destination_count);

                assert(left_cache->page.records_count == 0);

                key = old_middle->key = -1;
                struct btree_page_cache *new_cache = get_parent(tree, page_cache);

                remove_page(tree, left_cache);

                page_cache = new_cache;
                page = &page_cache->page;

                place = 0;
                for (; place < page->records_count && key != page->records[place].key; place++);

                goto remove;
            }
        }
        page_cache->access_time = cache_access_time++; // keep this page in memory

        if (right >= 0) {
            struct btree_page_cache *right_cache = read_btree_page(tree, right);
            if (right_cache->page.records_count == BTREE_D) {
                right_cache->modified = true;
                struct btree_record *old_middle = get_right_parent_record(tree, page_cache);
                assert(old_middle != NULL);

                int destination_count = (right_cache->page.records_count + page_cache->page.records_count + 1);
                assert(destination_count == 2 * BTREE_D);
                compensate(page_cache, old_middle, right_cache, NULL, -1, -1, false, destination_count);

                assert(page_cache->page.records_count == 0);

                key = old_middle->key = -1;

                struct btree_page_cache *new_cache = get_parent(tree, right_cache);

                remove_page(tree, page_cache);

                page_cache = new_cache;
                page = &page_cache->page;

                place = 0;
                for (; place < page->records_count && key != page->records[place].key; place++);

                goto remove;
            }
        }
    }
}

int count = 0;


void insert(struct btree *tree, struct btree_record *record) {
    //printf("Insert %d\n", record->key);
    struct btree_search_result search_result = search(tree, record->key);
    if (search_result.record != NULL) {
        //printf("Can not insert. Key exists\n");
        return;
    }

    count++;

    insert_precise(tree, record, search_result.page_cache, -1, -1);
}

void remove_record(struct btree *tree, int key) {
    //printf("Remove %d\n", key);
    struct btree_search_result search_result = search(tree, key);
    if (search_result.record == NULL) {
        //printf("Can not remove. Key not exists\n");
        return;
    }

    count--;

    remove_precise(tree, key, search_result.actual_cache);
}

int get_record_address(struct btree *tree, int key) {
    //printf("Update %d\n", key);
    struct btree_search_result search_result = search(tree, key);
    if (search_result.record == NULL) {
        printf("Can not update. Key not exists\n");
        return -1;
    }

    return search_result.record->address;
}

void update(struct btree *tree, struct tape *tape, int key, struct vector *v) {
    int address = get_record_address(tree, key);
    if (address >= 0) {
        update_record(tape, v, address);
    }
}

void print_page_link(int index) {
    if (index >= 0) {
        printf("(%d) ", index);
    } else {
        printf("(_) ");
    }
}

void print_btree_precise(struct btree *tree, int page) {
    if (page >= 0) {
        printf("Page %d:\n", page);
        struct btree_page_cache page_cache = *read_btree_page(tree, page);
        for (int i = 0; i < page_cache.page.records_count; i++) {
            if (!i) {
                print_page_link(page_cache.page.pointers[i]);
            }
            printf("%d ", page_cache.page.records[i].key);
            print_page_link(page_cache.page.pointers[i + 1]);
        }
        printf("\n");
        for (int i = 0; i < page_cache.page.records_count; i++) {
            if (!i) {
                print_btree_precise(tree, page_cache.page.pointers[i]);
            }
            print_btree_precise(tree, page_cache.page.pointers[i + 1]);
        }
    }
}

int print_last;
int print_count;

void print_btree_order_precise(struct btree *tree, struct tape *tape, int page) {
    if (page >= 0) {
        struct btree_page_cache page_cache = *read_btree_page(tree, page);
        for (int i = 0; i < page_cache.page.records_count; i++) {
            if (!i) {
                print_btree_order_precise(tree, tape, page_cache.page.pointers[i]);
            }

            assert(print_last < page_cache.page.records[i].key);
            print_last = page_cache.page.records[i].key;
            print_count++;

            struct vector *data = get_record(tape, page_cache.page.records[i].address);
            assert(data != NULL);

            printf("%d [%d, %d] ", page_cache.page.records[i].key, data->x, data->y);
            print_btree_order_precise(tree, tape, page_cache.page.pointers[i + 1]);
        }
    }
}

void print_btree(struct btree *tree) {
    printf("Print tree\n");
    print_btree_precise(tree, tree->head_index);
    printf("\n");
}

void print_btree_order(struct btree *tree, struct tape *tape) {
    printf("Print order tree\n");
    print_last = -1;
    print_count = 0;
    print_btree_order_precise(tree, tape, tree->head_index);
    printf("\nTotal %d\n\n", print_count);
}

int keys[1000000];

int main() {
    srand(time(0));

    printf("Records per page: %lu\n", RECORDS_PER_PAGE);
    printf("Page size: %d bytes\n", PAGE_SIZE);
    printf("Btree page size: %lu bytes\n", sizeof(struct btree_page));

    assert(RECORDS_PER_PAGE > 0);
    assert(sizeof(struct btree_page) <= PAGE_SIZE);

    struct btree tree = create_btree("tree.dat");
    struct tape *tape = prepare_file("tape.dat");
    erase_tape_and_start_writing(tape);
    reset_counters();

    struct btree_record record;
    struct vector data;

    {
        int mode;
        int s;

        printf("Podaj tryb:\n\t1 - sekwencja\n\t2 - ręczne operacje\n");
        s = scanf("%d", &mode);

        assert(s == 1 && mode >= 1 && mode <= 2);

        if (mode == 1) {
            int n;

            printf("Podaj liczbę rekordów n\n");
            s = scanf("%d", &n);
            assert(n > 0);

            for (int i = 0; i < n; i++) {
                keys[i] = rand();

                record.key = keys[i];
                record.address = tape->last_record + 1;

                data.x = rand() % 100;
                data.y = rand() % 100;

                insert(&tree, &record);
                push_record(tape, &data);
            }

            invalidate_cache(&tree);

            int a = get_read_page_counter(), b = get_write_page_counter(), c = btree_read_page_counter, d = btree_write_page_counter;

            printf("Plik seryjny - odczyty %d, zapisy %d\n", a, b);
            printf("B-drzewo - odczyty %d, zapisy %d\n", c, d);

            for (int i = 0; i < n; i++) {
                data.x = rand() % 100;
                data.y = rand() % 100;

                update(&tree, tape, keys[i], &data);
            }
            invalidate_cache(&tree);

            int e = get_read_page_counter(), f = get_write_page_counter(), g = btree_read_page_counter, h = btree_write_page_counter;

            printf("Plik seryjny - odczyty %d, zapisy %d\n", e - a, f - b);
            printf("B-drzewo - odczyty %d, zapisy %d\n", g - c, h - d);

            for (int i = 0; i < n; i++) {
                remove_record(&tree, keys[i]);
            }
            invalidate_cache(&tree);

            printf("Plik seryjny - odczyty %d, zapisy %d\n", get_read_page_counter() - e, get_write_page_counter() - f);
            printf("B-drzewo - odczyty %d, zapisy %d\n", btree_read_page_counter - g, btree_write_page_counter - h);

            /*
            print_btree(&tree);
            print_btree_order(&tree, tape);
            printf("count: %d\n", count);
             */
        } else {
            int operations;
            printf("Podaj liczbę operacji do wykonania\n");
            s = scanf("%d", &operations);
            assert(operations > 0);

            while (operations--) {
                int op;
                printf("Podaj rodzaj operacji:\n\t1 - dodawanie (key, x, y)\n\t2 - modyfikacja (key, x, y)\n\t3 - usuwanie (key)\n\t4 - wyświetlanie struktury ()\n\t5 - wyszukiwanie rekordu (key)\n");
                s = scanf("%d", &op);
                assert(op >= 1 && op <= 5);
                switch (op) {
                    case 1: {
                        record.address = tape->last_record + 1;

                        s = scanf("%d%d%d", &record.key, &data.x, &data.y);
                        assert(s == 3);

                        insert(&tree, &record);
                        push_record(tape, &data);
                    }
                        break;
                    case 2: {
                        s = scanf("%d%d%d", &record.key, &data.x, &data.y);
                        assert(s == 3);

                        update(&tree, tape, record.key, &data);
                    }
                        break;
                    case 3: {
                        s = scanf("%d", &record.key);
                        assert(s == 1);

                        remove_record(&tree, record.key);
                    }
                        break;
                    case 4: {
                        print_btree(&tree);
                        print_btree_order(&tree, tape);
                        printf("count: %d\n", count);
                    }
                        break;
                    case 5: {
                        s = scanf("%d", &record.key);
                        assert(s == 1);

                        struct btree_search_result result = search(&tree, record.key);
                        if (result.record != NULL && result.record->address >= 0) {
                            struct vector *v_data = get_record(tape, result.record->address);
                            if (v_data != NULL) {
                                printf("%d [%d, %d]\n", result.record->key, v_data->x, v_data->y);
                            }
                        }
                    }
                        break;
                }
            }
        }
    }

    close_file(tape);
    close_btree(&tree);

    return 0;
}
